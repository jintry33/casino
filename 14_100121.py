# This is a python file to show how the game works
#debug branch test /0104/2022
#debug branch test /0104/2022 -1
## main branch test /0104/2022
## main branch test /0104/2022 home
import random
import time
import string
from PyQt5.QtCore import reset
# import pandas as pd
# import plotly.express as px

from PyQt5.uic.uiparser import QtWidgets
#finalcard = list(range(9,110))
#fout_name=time.strftime("%Y%m%d-%H%M%S")+"_Bar.txt"
data_name=time.strftime("%Y%m%d-%H%M%S")+"_data.txt"
cardchoice=time.strftime("%Y%m%d-%H%M%S")+"card.txt"
test_time=time.strftime("%Y%m%d-%H%M%S")
OUTCOME = ['Player', 'Banker', 'Tie','Player_Natural','Banker_Natural','Player_Pander','Banker_Dragon']


import sys
from PyQt5.QtWidgets import *
from PyQt5 import uic
from PyQt5.QAxContainer import *
from PyQt5.QtGui import *

form_class = uic.loadUiType("bar_3.ui")[0]

class WindoeClass(QMainWindow, form_class):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.Shuffle.clicked.connect(self.cardchoice)
        self.Shuffle.clicked.connect(self.cardprint)
        self.CUT.clicked.connect(self.card.clear)
        self.CUT.clicked.connect(self.cutcard)
        self.BURN.clicked.connect(self.card.clear)
        self.BURN.clicked.connect(self.burncard)
        self.RUN.clicked.connect(self.play.clear)
        self.RUN.clicked.connect(self.run)
        self.RUN.clicked.connect(self.load)
        #time.sleep(3)
        self.RUN.clicked.connect(self.display)
    def cardchoice(self):

    #카드 8벌을 1번부터 416으로 번호 정하기
        global c
        c = list(range(1,417))
        #카드 8벌을 섞기
        random.shuffle(c)
        print (c)

        # 1~13으로 구분하기
        for index, value in enumerate(c):
            for i in range(416):
                if index == i:
                    c[index]=c[index]%13
                    if c[index] > 9:
                        #10이상의 수는 0으로 환산
                        c[index]=0
        print (c)
        global c1
        c1 = " ".join(str(x) for x in c)
        print (c1)
        # for i in range(10):
        #     b = c.count (i)
        #     print (i ,"counter is ", b)
    def cardprint(self):
        self.card.setText(c1+"\t")
    def cutcard(self):
    #CUT 설정
        global aftercut
        global cut
        self.cut = self.lineEdit_cut.text()
        cut = int(self.cut)
        print(self.cut)
        print(cut)
        aftercut_0 = c[cut:]
        aftercut_1 = c[0:cut]
        aftercut = aftercut_0+ aftercut_1
        print (aftercut)
        aftercutc =len(aftercut)
        print("aftercut.count is",aftercutc)
        print()
        aftercut_1 = " ".join(str(x) for x in aftercut)
        self.card.setText(aftercut_1+"\t")
        # for i in range(10):
        #     b = aftercut.count (i)
        #     print (i ,"counter is ", b)

    def burncard(self):
        #BURN CARD 설정
        burncard = aftercut[0]
        if burncard == 0:
            burncard = 10
        print ("Burn card is",burncard)
        burncard_1 = str(burncard)
        self.label_burn.setText(burncard_1)
        # burncard_2 = " ".join(str(x) for x in aftercut)
        # self.card.setText(burncard_2+"\t")
        global finalcard
        finalcard = aftercut[burncard+1:]
        finalcard_1 = " ".join(str(x) for x in finalcard)
        self.card.setText(finalcard_1+"\t")
        print("1st final card is ",finalcard)
        finalcardc =len(finalcard)
        print("1st finalcard.count is",finalcardc)

        f = open(data_name, 'a') # 파일 열기
        print("original card is","\r","\n", c,"\r","\n","cut card is","\t",self.cut,"\r","\n","after cut card is","\r","\n",aftercut,"\r","\n","burn card is ", burncard,"\r","\n", finalcard,"\r","\n",file=f) # 파일 저장하기
        f.close()
    #바카라 계산

    def run(self):
        # Inclusive range function
        irange = lambda start, end: range(start, end + 1)

        while len(finalcard) >=20:
            finalcardc = len(finalcard)
            #print("2nd finalcard.count is",finalcardc)
            def compute_score(hand):
                """Compute the score of a hand"""
                total_value = 0
                for card in hand:
                    total_value += card
                return total_value % 10
                #Has 3rd card?
                #print ("player has ##### ", len(player_hand))
                #print ("Banker has ##### ", len(banker_hand))

            def play():
                """Returns the winner"""
                global finalcard #지역변수 지정
                #print("2nd final card is ",finalcard)
                #time.sleep(0.1)
                player_hand = [
                    finalcard[0],
                    finalcard[2]
                ]
                banker_hand = [
                    finalcard[1],
                    finalcard[3]
                ]
                player_score = compute_score(player_hand)
                banker_score = compute_score(banker_hand)


                print('Player has cards:\t' , finalcard[0] , '\t' , finalcard[2])
                print('Player has score of\t' + str(player_score))
                print('Banker has cards:\t' , finalcard[1] , '\t' , finalcard[3])
                print('Banker has score of\t' + str(banker_score))
                cut1 = str(cut)

                global fout_name
                fout_name=time.strftime("%Y%m%d-%H%M%S")+"cut_"+cut1+".txt"
                # f = open(fout_name, 'a') # 파일 열기
                # print("player[0]","\t","player[1]","\t","player[2]","\t","player_score","\t","\t","banker[0]","\t","banker[1]","\t","banker[2]","\t","banker_score","\t","Result", file=f) # 파일 저장하기
                # f.close()
                # Natural
                if player_score in [8, 9] or banker_score in [8, 9]:
                    finalcard = finalcard[4:]
                    if player_score > banker_score:
                        f = open(fout_name, 'a') # 파일 열기
                        print(player_hand[0],"\t",player_hand[1],"\t","X","\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t","X","\t",banker_score,"\t",OUTCOME[3], file=f) # 파일 저장하기
                        f.close()
                        return OUTCOME[3]
                    elif player_score < banker_score:
                        f = open(fout_name, 'a') # 파일 열기
                        print(player_hand[0],"\t",player_hand[1],"\t","X","\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t","X","\t",banker_score,"\t",OUTCOME[4], file=f) # 파일 저장하기
                        f.close()
                        return OUTCOME[4]
                    elif player_score == banker_score:
                        f = open(fout_name, 'a') # 파일 열기
                        print(player_hand[0],"\t",player_hand[1],"\t","X","\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t","X","\t",banker_score,"\t",OUTCOME[2], file=f) # 파일 저장하기
                        f.close()
                        return OUTCOME[2]

                #print (finalcard)
                    # Player has low score
                if player_score in irange(0, 5):
                    # Player get's a third card
                    player_hand = [
                        finalcard[0],
                        finalcard[2],
                        finalcard[4]
                    ]
                    player_third = finalcard[4]
                    print('Player gets a third card:\t' , finalcard[4])
                    #finalcard = finalcard[5:]
                    # Determine if banker needs a third card
                    if (banker_score == 6 and player_third in [6, 7]) or \
                    (banker_score == 5 and player_third in irange(4, 7)) or \
                    (banker_score == 4 and player_third in irange(2, 7)) or \
                    (banker_score == 3 and player_third != 8) or \
                    (banker_score in [0, 1, 2]):
                        banker_hand = [
                        finalcard[1],
                        finalcard[3],
                        finalcard[5]
                        ]
                        print('Banker gets a third card:\t' , finalcard[5])
                        finalcard = finalcard[6:]
                    else:
                        finalcard = finalcard[5:]
                elif player_score in [6, 7]:
                    if banker_score in irange(0, 5):
                        #banker_hand.append(random.choice(CARDS))
                        banker_hand = [
                        finalcard[1],
                        finalcard[3],
                        finalcard[4]
                        ]
                        print('Banker gets a third card:\t' , finalcard[5])
                        finalcard = finalcard[5:]
                    else:
                        finalcard = finalcard[4:]
                else:
                    finalcard = finalcard[5:]
                #Has 3rd card?
                print ("player has ### ", len(player_hand),"cards")
                print ("Banker has ### ", len(banker_hand),"cards")

                # Compute the scores again and return the outcome
                player_score = compute_score(player_hand)
                banker_score = compute_score(banker_hand)

                print('Player has final score of\t' + str(player_score))
                print('Banker has final score of\t' + str(banker_score))

                if player_score > banker_score and player_score == 8 and len(player_hand)==3 and len(banker_hand)==2:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t",player_hand[2],"\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t","X","\t",banker_score,"\t",OUTCOME[5],"\t", file=f) # 파일 저장하기
                    f.close()         
                    return OUTCOME[5]
                if player_score > banker_score and player_score == 8 and len(player_hand)==3 and len(banker_hand)==3:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t",player_hand[2],"\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t",banker_hand[2],"\t",banker_score,"\t",OUTCOME[5],"\t", file=f) # 파일 저장하기
                    f.close()         
                    return OUTCOME[5]   
                elif player_score < banker_score and banker_score == 7 and len(banker_hand)==3 and len(player_hand)==3:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t",player_hand[2],"\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t", banker_hand[2],"\t",banker_score,"\t",OUTCOME[6],"\t",file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[6]
                elif player_score < banker_score and banker_score == 7 and len(banker_hand)==3 and len(player_hand)==2:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t","X","\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t", banker_hand[2],"\t",banker_score,"\t",OUTCOME[6],"\t",file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[6]
                elif player_score > banker_score and len(banker_hand)==3 and len(player_hand)==3:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t",player_hand[2],"\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t", banker_hand[2],"\t",banker_score,"\t",OUTCOME[0],"\t",file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[0]
                elif player_score > banker_score and len(banker_hand)==2 and len(player_hand)==3:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t",player_hand[2],"\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t","X","\t",banker_score,"\t",OUTCOME[0],"\t",file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[0]    
                elif player_score > banker_score and len(banker_hand)==3 and len(player_hand)==2:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t","X","\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t",banker_hand[2],"\t",banker_score,"\t",OUTCOME[0],"\t",file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[0]     
                elif player_score < banker_score and len(banker_hand)==3 and len(player_hand)==3:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t",player_hand[2],"\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t", banker_hand[2],"\t",banker_score,"\t",OUTCOME[1],"\t",file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[1]
                elif player_score < banker_score and len(banker_hand)==2 and len(player_hand)==3:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t",player_hand[2],"\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t","X","\t",banker_score,"\t",OUTCOME[1],"\t",file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[1]
                elif player_score < banker_score and len(banker_hand)==3 and len(player_hand)==2:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t","X","\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t", banker_hand[2],"\t",banker_score,"\t",OUTCOME[1],"\t",file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[1]
                elif player_score == banker_score and len(banker_hand)==3 and len(player_hand)==3:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t",player_hand[2],"\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t", banker_hand[2],"\t",banker_score,"\t",OUTCOME[2],file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[2]
                elif player_score == banker_score and len(banker_hand)==2 and len(player_hand)==3:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t",player_hand[2],"\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t", "X","\t",banker_score,"\t",OUTCOME[2],file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[2]
                elif player_score == banker_score and len(banker_hand)==3 and len(player_hand)==2:
                    f = open(fout_name, 'a') # 파일 열기
                    print(player_hand[0],"\t",player_hand[1],"\t","X","\t",player_score,"\t","\t",banker_hand[0],"\t",banker_hand[1],"\t", banker_hand[2],"\t",banker_score,"\t",OUTCOME[2],file=f) # 파일 저장하기
                    f.close()
                    return OUTCOME[2]
                finalcard = finalcard[6:]
                #time.sleep(0.05)
            print(play())

    def load(self):
        f = open(fout_name, 'r')
        data = f.read()
        print(data)

        #갯수 세기
        Player = (data.count('Player')+data.count('Player_Pander'))
        Banker = (data.count('Banker')+data.count('Banker_Dragon'))
        Tie = (data.count('Tie'))
        Dragon = (data.count('Banker_Dragon'))
        Pander = (data.count('Player_Pander'))
        Games = Player+Banker+Tie+Dragon+Pander

        # % 계산
        Player_s = round ((data.count('Player')+data.count('Player_Pander'))/Games*100,1)
        Banker_s = round ((data.count('Banker')+data.count('Banker_Dragon'))/Games*100,1)
        Tie_s = round ((data.count('Tie'))/Games*100,1)
        Dragon_s = round ((data.count('Banker_Dragon'))/Games*100,1)
        Pander_s = round ((data.count('Player_Pander'))/Games*100,1)
        Games_s = (Player+Banker+Tie+Dragon+Pander)/Games*100

        # 결과 갯수 세기
        # Player_1 = str(Player)
        # Banker_1 = str(Banker)
        # Tie_1 = str(Tie)
        # Dragon_1 = str(Dragon)
        # Pander_1 = str(Pander)
        # Games_1 = str(Games)


        # 결과 % 계산
        # Player_2 = str(Player_s)
        # Banker_2 = str(Banker_s)
        # Tie_2 = str(Tie_s)
        # Dragon_2 = str(Dragon_s)
        # Pander_2 = str(Pander_s)
        # Games_2 = str(Games_s)

        # 갯수 표시
        self.label_12.setText(str(Player))
        self.label_10.setText(str(Banker))
        self.label_9.setText(str(Tie))
        self.label_8.setText(str(Dragon))
        self.label_7.setText(str(Pander))
        self.label_11.setText(str(Games))

        # % 표시
        self.label_16.setText(str(Player_s))
        self.label_17.setText(str(Banker_s))
        self.label_15.setText(str(Tie_s))
        self.label_18.setText(str(Dragon_s))
        self.label_14.setText(str(Pander_s))
        self.label_13.setText(str(Games_s))

        self.play.setText(data)
        f.close()

    def display(self):
        f = open(fout_name,"r")
        lines=f.readlines()
        result=[]
        for x in lines:
            result.append(x.split()[8])
        f.close()
        print (result)
        
        count = len(result)
        print("result count is",count)

        x=20
        y=290


        for i in range(0,count):
            print(i,result[i])
            results=result[i]
            result_0=result[i-1]
            result_1=result[i-2]
            result_2=result[i-3]
            startwith = results [:6]
            startwith_0 = result_0 [:6]
            startwith_1 = result_1 [:6]
            startwith_2 = result_2 [:6]
            print (startwith)
            if i == 0:
                exec(f'self.R{i}.move (x,y)')
            elif startwith_0 == startwith or result[i] =="Tie":
                y=y+30
                exec(f'self.R{i}.move (x,y)')
            elif startwith_0 != startwith and startwith_0 =="Tie" and startwith == startwith_1:
                y=y+30
                exec(f'self.R{i}.move (x,y)')
            elif startwith_0 != startwith and startwith_1 =="Tie" and startwith == startwith_2:
                y=y+30
                exec(f'self.R{i}.move (x,y)')
            elif startwith_0 != startwith:
                x=x+30
                y = 290
                exec(f'self.R{i}.move (x,y)')

        for i in range(0,count):
            if result[i] == "Player":
                exec(f'self.R{i}.setStyleSheet("background-color: lightblue")')
                exec(f'self.R{i}.setText("P")')
            elif result[i] == "Player_Natural":
                exec(f'self.R{i}.setStyleSheet("background-color: lightblue")')
                exec(f'self.R{i}.setText("PN")')
            elif result[i] == "Player_Pander":
                exec(f'self.R{i}.setStyleSheet("background-color: blue")')
                exec(f'self.R{i}.setText("PP")')
            elif result[i] == "Banker":
                exec(f'self.R{i}.setStyleSheet("background-color: red")')
                exec(f'self.R{i}.setText("B")')
            elif result[i] == "Banker_Natural":
                exec(f'self.R{i}.setStyleSheet("background-color: red")')
                exec(f'self.R{i}.setText("BN")')
            elif result[i] == "Banker_Dragon":
                exec(f'self.R{i}.setStyleSheet("background-color: yellow")')
                exec(f'self.R{i}.setText("BD")')
            elif result[i] == "Tie":
                exec(f'self.R{i}.setStyleSheet("background-color: green")')
                exec(f'self.R{i}.setText("T")')



if __name__ == "__main__" :
    #QApplication : 프로그램을 실행시켜주는 클래스
    app = QApplication(sys.argv)

    #WindowClass의 인스턴스 생성
    myWindow = WindoeClass()

    #프로그램 화면을 보여주는 코드
    myWindow.show()

    #프로그램을 이벤트루프로 진입시키는(프로그램을 작동시키는) 코드
    app.exec_()